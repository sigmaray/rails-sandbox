require "test_helper"

class GuestbookMessagesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @guestbook_message = guestbook_messages(:one)
  end

  test "should get index" do
    get guestbook_messages_url
    assert_response :success
  end

  test "should get new" do
    get new_guestbook_message_url
    assert_response :success
  end

  test "should create guestbook_message" do
    assert_difference("GuestbookMessage.count") do
      post guestbook_messages_url, params: { guestbook_message: { ip: @guestbook_message.ip, name: @guestbook_message.name, remote_ip: @guestbook_message.remote_ip, text: @guestbook_message.text, user_agent: @guestbook_message.user_agent } }
    end

    assert_redirected_to guestbook_message_url(GuestbookMessage.last)
  end

  test "should show guestbook_message" do
    get guestbook_message_url(@guestbook_message)
    assert_response :success
  end

  test "should get edit" do
    get edit_guestbook_message_url(@guestbook_message)
    assert_response :success
  end

  test "should update guestbook_message" do
    patch guestbook_message_url(@guestbook_message), params: { guestbook_message: { ip: @guestbook_message.ip, name: @guestbook_message.name, remote_ip: @guestbook_message.remote_ip, text: @guestbook_message.text, user_agent: @guestbook_message.user_agent } }
    assert_redirected_to guestbook_message_url(@guestbook_message)
  end

  test "should destroy guestbook_message" do
    assert_difference("GuestbookMessage.count", -1) do
      delete guestbook_message_url(@guestbook_message)
    end

    assert_redirected_to guestbook_messages_url
  end
end
