require "application_system_test_case"

class GuestbookMessagesTest < ApplicationSystemTestCase
  setup do
    @guestbook_message = guestbook_messages(:one)
  end

  test "visiting the index" do
    visit guestbook_messages_url
    assert_selector "h1", text: "Guestbook messages"
  end

  test "should create guestbook message" do
    visit guestbook_messages_url
    click_on "New guestbook message"

    fill_in "Ip", with: @guestbook_message.ip
    fill_in "Name", with: @guestbook_message.name
    fill_in "Remote ip", with: @guestbook_message.remote_ip
    fill_in "Text", with: @guestbook_message.text
    fill_in "User agent", with: @guestbook_message.user_agent
    click_on "Create Guestbook message"

    assert_text "Guestbook message was successfully created"
    click_on "Back"
  end

  test "should update Guestbook message" do
    visit guestbook_message_url(@guestbook_message)
    click_on "Edit this guestbook message", match: :first

    fill_in "Ip", with: @guestbook_message.ip
    fill_in "Name", with: @guestbook_message.name
    fill_in "Remote ip", with: @guestbook_message.remote_ip
    fill_in "Text", with: @guestbook_message.text
    fill_in "User agent", with: @guestbook_message.user_agent
    click_on "Update Guestbook message"

    assert_text "Guestbook message was successfully updated"
    click_on "Back"
  end

  test "should destroy Guestbook message" do
    visit guestbook_message_url(@guestbook_message)
    click_on "Destroy this guestbook message", match: :first

    assert_text "Guestbook message was successfully destroyed"
  end
end
