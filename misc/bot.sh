#!/bin/bash

# URL of the HTTP form
url='http://localhost:3088/guestbook_messages'

# Function to post fake data to the form
post_fake_data() {
    echo "Posting data"

    name="bash/curl"
    text=$(date +"%Y_%b_%d__%H_%M_%S")

    # Post the data to the form using curl
    response=$(curl 'http://localhost:3088/guestbook_messages' --data-raw "guestbook_message%5Bname%5D=$name&guestbook_message%5Btext%5D=$text")

    # Print the response status code for debugging
    echo "Response status code: $response"
}

# Main loop to post data every minute
while true; do
    post_fake_data
    sleep 60  # Sleep for 60 seconds
done
