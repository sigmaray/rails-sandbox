import time
import requests
from faker import Faker

# Initialize Faker
fake = Faker()

# URL of the HTTP form
url = 'http://localhost:3088/guestbook_messages'

# Function to generate fake data and post to the form
def post_fake_data():
    # Generate fake data
    fake_data = {
        'guestbook_message[name]': "python/requests/" + fake.name(),
        'guestbook_message[text]': fake.text(),
    }
    
    # Print the data for debugging
    print(f"Posting data: {fake_data}")
    
    # Post the data to the form
    response = requests.post(url, data=fake_data)
    
    # Print the response for debugging
    print(f"Response status code: {response.status_code}")

# Main loop to post data every minute
while True:
    post_fake_data()
    time.sleep(60)  # Sleep for 60 seconds
