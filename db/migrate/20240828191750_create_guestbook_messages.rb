class CreateGuestbookMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :guestbook_messages do |t|
      t.string :name
      t.text :text
      t.string :ip
      t.string :remote_ip
      t.string :user_agent

      t.timestamps
    end
  end
end
