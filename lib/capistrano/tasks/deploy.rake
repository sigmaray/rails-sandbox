# Restart the container using docker-compose
task :docker_restart do
  on roles(:app) do
    within release_path do
      execute "docker compose -f #{fetch(:deploy_to)}/current/compose.staging.yml down"
      execute "docker compose -f #{fetch(:deploy_to)}/current/compose.staging.yml up -d"
    end
  end
end

after :deploy, :docker_restart
