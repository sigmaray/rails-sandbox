class GuestbookMessagesController < InheritedResources::Base
  # Disable form token verification
  skip_before_action :verify_authenticity_token

  actions :all, :except => [ :show, :edit, :update, :destroy ]

  def create
    @guestbook_message = GuestbookMessage.new(guestbook_message_params)
    @guestbook_message.ip = request.ip
    @guestbook_message.remote_ip = request.remote_ip
    @guestbook_message.user_agent = request.user_agent
    create!
  end

  private

    def guestbook_message_params
      params.require(:guestbook_message).permit(:name, :text)
    end

    # https://github.com/activeadmin/inherited_resources#:~:text=want%20to%20add-,pagination,-to%20your%20projects
    def collection
      get_collection_ivar || set_collection_ivar(end_of_association_chain.order('created_at desc').page(params[:page]))
    end

end
