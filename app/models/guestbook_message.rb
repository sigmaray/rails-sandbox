class GuestbookMessage < ApplicationRecord
  paginates_per 10

  validates :text, presence: true
end
