ActiveAdmin.register GuestbookMessage do
  permit_params :name, :text, :ip, :remote_ip, :user_agent

  action_item :import_demo, only: :index do
    link_to 'Generate (1)', generate_admin_guestbook_messages_path(count: 1)
  end

  action_item :import_demo, only: :index do
    link_to 'G (100)', generate_admin_guestbook_messages_path(count: 100)
  end

  action_item :import_demo, only: :index do
    link_to 'G (1_000)', generate_admin_guestbook_messages_path(count: 1_000)
  end

  action_item :import_demo, only: :index do
    link_to 'G (10_000)', generate_admin_guestbook_messages_path(count: 10_000)
  end

  action_item :delete_all, only: :index do
    link_to 'Delete All', delete_all_admin_guestbook_messages_path
  end

  collection_action :generate, method: :get do
    count = params[:count].to_i
    count = 1 if count == 0
    count.times do |i|
      text = "Generated in ActiveAdmin #{i}"
      GuestbookMessage.create(name: 'ActiveAdmin', text:)
    end
    redirect_to collection_path
  end

  collection_action :delete_all, method: :get do
    GuestbookMessage.delete_all
    redirect_to collection_path
  end

  # See permitted parameters documentation:
  # https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # Uncomment all parameters which should be permitted for assignment
  #
  # permit_params :name, :text, :ip, :remote_ip, :user_agent
  #
  # or
  #
  # permit_params do
  #   permitted = [:name, :text, :ip, :remote_ip, :user_agent]
  #   permitted << :other if params[:action] == 'create' && current_user.admin?
  #   permitted
  # end
  
end
