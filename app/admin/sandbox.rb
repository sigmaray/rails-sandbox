ActiveAdmin.register_page "Sandbox" do
  content do
    div style: 'margin: 1rem 0;' do
      link_to 'HardJob.perform_async', admin_sandbox_perform_async_path       
    end

    div style: 'margin: 1rem 0;' do
      link_to 'HardJob.new.perform', admin_sandbox_perform_sync_path       
    end
  end

  page_action :perform_async, method: :get do
    HardJob.perform_async
    redirect_to admin_sandbox_path, notice: "HardJob.perform_async"
  end

  page_action :perform_sync, method: :get do
    HardJob.new.perform
    redirect_to admin_sandbox_path, notice: "HardJob.new.perform"
  end
end