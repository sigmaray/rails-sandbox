json.extract! guestbook_message, :id, :name, :text, :ip, :remote_ip, :user_agent, :created_at, :updated_at
json.url guestbook_message_url(guestbook_message, format: :json)
