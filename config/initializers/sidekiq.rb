# frozen_string_literal: true
redis_host = File.exists?('/.dockerenv') ? 'sandbox_redis' : 'localhost'
sidekiq_config = { url: "redis://#{redis_host}:6379/12" }
Sidekiq.configure_server do |config|
  config.redis = sidekiq_config
end
Sidekiq.configure_client do |config|
  config.redis = sidekiq_config
end
